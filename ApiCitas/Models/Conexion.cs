﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace ApiCitas.Models
{
    public static class Conexion
    {
        
        

        public static SqlConnection getConexion()
        {
            string connectionString = @"Data Source=" + ConfigurationManager.AppSettings["dbserver"] + ";Initial Catalog=" + ConfigurationManager.AppSettings["db"] + ";User ID=" + ConfigurationManager.AppSettings["user"] + ";Password=" + ConfigurationManager.AppSettings["pass"];
            SqlConnection cnn = new SqlConnection(connectionString);
            cnn.Open();
            return cnn;
        }

        public static void cerrarConexion(SqlConnection conn)
        {
            conn.Close();
        }
    }
}