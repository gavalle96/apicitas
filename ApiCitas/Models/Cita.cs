﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ApiCitas.Models
{
    public class Cita
    {
        public int IdCita { get; set; }
        public DateTime FechaHoraCita { get; set; }
        public int IdPaciente { get; set; }
        public string NomPaciente { get; set; }
        public string ApePaciente { get; set; }
        public int IdDoctor { get; set; }
        public string NomDoctor { get; set; }
        public string ApeDoctor { get; set; }
        public string Especialidad { get; set; }
        public string EstadoCita { get; set; }
        public string Observaciones { get; set; }


    }
}