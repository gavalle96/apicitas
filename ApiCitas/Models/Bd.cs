﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
namespace ApiCitas.Models
{
    public static class Bd
    {
        #region Citas
        public static Tuple<bool, string, List<Cita>> consultarCitas()
        {
            try
            {
                List<Cita> retorno = new List<Cita>();
                using (SqlConnection conn = Conexion.getConexion())
                {
                    SqlCommand cmd = new SqlCommand("ConsultarCitas", conn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    using (SqlDataReader rdr = cmd.ExecuteReader())
                    {
                        while (rdr.Read())
                        {
                            
                            retorno.Add(new Cita() {
                                IdCita = (int)rdr["IdCita"],
                                FechaHoraCita = (DateTime)rdr["FechaHoraCita"],
                                IdPaciente = (int)rdr["IdPaciente"],
                                NomPaciente = (string)rdr["NomPaciente"],
                                ApePaciente = (string)rdr["ApePaciente"],
                                IdDoctor = (int)rdr["IdDoctor"],
                                NomDoctor = (string)rdr["NomDoctor"],
                                ApeDoctor = (string)rdr["ApeDoctor"],
                                Especialidad = (string)rdr["Especialidad"],
                                EstadoCita = (string)rdr["EstadoCita"]

                            }
                            );
                        }
                    }

                    conn.Close();
                    return new Tuple<bool, string, List<Cita>>(true, "Acción completada correctamente", retorno);

                }

            }
            catch (Exception e)
            {
                Console.WriteLine("Error en método de consulta de citas", e.ToString());
                return new Tuple<bool, string, List<Cita>>(false, "Ocurrió un error al realizar la acción solicitada", new List<Cita>());
            }
        }

        public static Tuple<bool, string, Cita> ObtenerCita(int IdCita)
        {
            try
            {
                Cita retorno = new Cita();
                using (SqlConnection conn = Conexion.getConexion())
                {
                    SqlCommand cmd = new SqlCommand("ObtenerCita", conn);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@IdCita", IdCita));

                    using (SqlDataReader rdr = cmd.ExecuteReader())
                    {
                        if (rdr.Read())
                        {

                            retorno = new Cita()
                            {
                                IdCita = (int)rdr["IdCita"],
                                FechaHoraCita = (DateTime)rdr["FechaHoraCita"],
                                IdPaciente = (int)rdr["IdPaciente"],
                                NomPaciente = (string)rdr["NomPaciente"],
                                ApePaciente = (string)rdr["ApePaciente"],
                                IdDoctor = (int)rdr["IdDoctor"],
                                NomDoctor = (string)rdr["NomDoctor"],
                                ApeDoctor = (string)rdr["ApeDoctor"],
                                Especialidad = (string)rdr["Especialidad"],
                                EstadoCita = (string)rdr["EstadoCita"],
                                Observaciones = (string)(rdr.IsDBNull(rdr.GetOrdinal("Observaciones")) ? "" : rdr["Observaciones"])

                            };
                            
                        }
                    }

                    conn.Close();
                    return new Tuple<bool, string, Cita>(true, "Acción completada correctamente", retorno);

                }

            }
            catch (Exception e)
            {
                Console.WriteLine("Error en método de consulta de citas", e.ToString());
                return new Tuple<bool, string, Cita>(false, "Ocurrió un error al realizar la acción solicitada", new Cita());
            }
        }

        public static Tuple<bool, string> CrearCita(Cita cita)
        {
            try
            {
                Cita retorno = new Cita();
                using (SqlConnection conn = Conexion.getConexion())
                {
                    SqlCommand cmd = new SqlCommand("CrearCita", conn);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@IdPaciente", cita.IdPaciente));
                    cmd.Parameters.Add(new SqlParameter("@IdDoctor", cita.IdDoctor));
                    cmd.Parameters.Add(new SqlParameter("@FechaHoraCita", cita.FechaHoraCita));

                    cmd.ExecuteNonQuery();


                    conn.Close();
                    return new Tuple<bool, string>(true, "Acción completada correctamente");

                }

            }
            catch (Exception e)
            {
                Console.WriteLine("Error en método de creacion de citas", e.ToString());
                return new Tuple<bool, string>(false, "Ocurrió un error al realizar la acción solicitada");
            }
        }

        public static Tuple<bool, string> ModificarCita(Cita cita)
        {
            try
            {
                Cita retorno = new Cita();
                using (SqlConnection conn = Conexion.getConexion())
                {
                    SqlCommand cmd = new SqlCommand("ModificarCita", conn);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@IdCita", cita.IdCita));
                    cmd.Parameters.Add(new SqlParameter("@IdDoctor", cita.IdDoctor));
                    cmd.Parameters.Add(new SqlParameter("@FechaHoraCita", cita.FechaHoraCita));

                    cmd.ExecuteNonQuery();


                    conn.Close();
                    return new Tuple<bool, string>(true, "Acción completada correctamente");

                }

            }
            catch (Exception e)
            {
                Console.WriteLine("Error en método de modificar de citas", e.ToString());
                return new Tuple<bool, string>(false, "Ocurrió un error al realizar la acción solicitada");
            }
        }

        public static Tuple<bool, string> EliminarCita(int IdCita)
        {
            try
            {
                Cita retorno = new Cita();
                using (SqlConnection conn = Conexion.getConexion())
                {
                    SqlCommand cmd = new SqlCommand("EliminarCita", conn);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@IdCita", IdCita));
                    cmd.ExecuteNonQuery();


                    conn.Close();
                    return new Tuple<bool, string>(true, "Acción completada correctamente");

                }

            }
            catch (Exception e)
            {
                Console.WriteLine("Error en método de elimimar de citas", e.ToString());
                return new Tuple<bool, string>(false, "Ocurrió un error al realizar la acción solicitada");
            }
        }

        public static Tuple<bool, string> AsignarObservacionCita(int IdCita, string Observaciones)
        {
            try
            {
                Cita retorno = new Cita();
                using (SqlConnection conn = Conexion.getConexion())
                {
                    SqlCommand cmd = new SqlCommand("AsignarObservacionesCita", conn);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@IdCita", IdCita));
                    cmd.Parameters.Add(new SqlParameter("@Observaciones", Observaciones));
                    cmd.ExecuteNonQuery();


                    conn.Close();
                    return new Tuple<bool, string>(true, "Acción completada correctamente");

                }

            }
            catch (Exception e)
            {
                Console.WriteLine("Error en método de asignar observaciones de citas", e.ToString());
                return new Tuple<bool, string>(false, "Ocurrió un error al realizar la acción solicitada");
            }
        }
        #endregion
    }
}