﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using ApiCitas.Models;

namespace ApiCitas.Controllers
{
    public class CitasController : ApiController
    {
        [HttpGet]
        [Route("api/Citas/Consultar")]
        public Tuple<bool, string, List<Cita>> Consultar()
        {
            return Bd.consultarCitas();
        }

        [HttpGet]
        [Route("api/Citas/ObtenerCita")]
        public Tuple<bool, string, Cita> ConsultarCita([FromUri] int IdCita)
        {
            return Bd.ObtenerCita(IdCita);
        }

        [HttpPost]
        [Route("api/Citas/Crear")]
        public Tuple<bool, string> CrearCita([FromBody] Cita cita)
        {
            return Bd.CrearCita(cita);
        }

        [HttpPost]
        [Route("api/Citas/Modificar")]
        public Tuple<bool, string> ModificarCita([FromBody] Cita cita)
        {
            return Bd.ModificarCita(cita);
        }

        [HttpPost]
        [Route("api/Citas/AsignarObservaciones")]
        public Tuple<bool, string> AsignarObservacionesCita([FromBody] Cita cita)
        {
            return Bd.AsignarObservacionCita(cita.IdCita, cita.Observaciones);
        }

        [HttpDelete]
        [Route("api/Citas/Eliminar")]
        public Tuple<bool, string> EliminarCita([FromUri] int IdCita)
        {
            return Bd.EliminarCita(IdCita);
        }
    }
}
